package br.com.microfriends.secondmaven.domain;

import java.util.Objects;

public class Fundo {

	public Double validarValor(Double valor) {
		if (Objects.isNull(valor)) {
			return 0D;
		} else if (valor > 0) {
			return 10D;
		} else {
			return valor * 2;
		}
	}
}
