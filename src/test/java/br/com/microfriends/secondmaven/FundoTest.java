package br.com.microfriends.secondmaven;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import br.com.microfriends.secondmaven.domain.Fundo;

public class FundoTest {

	@Test
	public void deve_retornar_10_se_valor_maior_que_zero() {
		Fundo fundo = new Fundo();
		Double valorRetornado = fundo.validarValor(1D);
		assertEquals(10D, valorRetornado, 0);
	}
	
	@Test
	public void teste_se_valor_menor_que_zero() {
		Fundo fundo = new Fundo();
		Double valorRetornado = fundo.validarValor(-2D);
		assertEquals(-4D, valorRetornado, 0);
	}
	
	@Test
	public void deve_retornar_zero_se_null() {
		Fundo fundo = new Fundo();
		Double valorRetornado = fundo.validarValor(null);
		assertEquals(0D, valorRetornado, 0);
	}
}
